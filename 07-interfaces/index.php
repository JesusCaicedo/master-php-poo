<?php

//contrato done se de definen metodos van a tener las clases


interface Ordenador{
    public function encender();
    public function apagar();
    public function reiniciar();
    public function desfragmentar();
}

class iMac implements Ordenador{
    public $modelo;

    public function getModelo(){
        return $this->modelo;
    }
    public function setModelo($modelo){
        $this->modelo=$modelo;
    }

    public function encender(){
        return "lo que sea";
    }
    public function apagar(){
        return "lo que sea";
    }
    public function reiniciar(){  
        return "lo que sea";
    }
    public function desfragmentar(){
        return "lo que sea";
    }

}

$mac = new iMac();
$mac ->setModelo('mac pro 2021');
echo $mac->getModelo();
echo $mac->encender();