<?php

class UsuarioController{

    public function mostrarTodos(){

        require_once 'models/usuarioModels.php';

        $usuario=new Usuario();
        $todos_los_usarios = $usuario->conseguirTodos('usuarios');

        require_once 'views/usuarios/mostrar-todos.php';


    }
    public function Crear(){
        require_once 'views/usuarios/crear.php';
    }

}

?>