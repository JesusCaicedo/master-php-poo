<h1>Bienvenido a la Web</h1>
<?php

require_once 'autoload.php';



if(isset($_GET['controller'])){
    $nombre_controlador = $_GET['controller'].'Controller';
}else{
     echo "<h1> La pagina no existe </h1>";
     exit();

}



//si por url llegr el controlador  y la clase existe
if(class_exists($nombre_controlador)){

    //guardo el controlador que llega por la url
    $controlador =new $nombre_controlador();

    if (isset($_GET['action']) && method_exists($controlador, $_GET['action'])){
        $action = $_GET['action'];
            $controlador->$action();
    }else{
        echo "<h1> La pagina no existe </h1>";
}
}else{
        echo "<h1> Controlador invalido </h1>";

}




?>