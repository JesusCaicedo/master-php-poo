<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'bfc49a0d53bc80c99ab476afd5a31a493defb188',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'bfc49a0d53bc80c99ab476afd5a31a493defb188',
    ),
    'firephp/firephp-core' => 
    array (
      'pretty_version' => 'v0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '2585b6868fe7d3a9d1432b738c3cc547444d0348',
    ),
    'spipu/html2pdf' => 
    array (
      'pretty_version' => 'v5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e6d8ca22347b6691bb8c2652212b1be2c89b3eff',
    ),
    'stefangabos/zebra_pagination' => 
    array (
      'pretty_version' => '2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4a003961cfb21c628a299cefee070f15ed504e67',
    ),
    'tecnickcom/tcpdf' => 
    array (
      'pretty_version' => '6.4.1',
      'version' => '6.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5ba838befdb37ef06a16d9f716f35eb03cb1b329',
    ),
  ),
);
