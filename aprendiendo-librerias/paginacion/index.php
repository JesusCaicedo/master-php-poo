<?php

require '../vendor/autoload.php';

//conexion db
$conexion = new mysqli("localhost", "root", "", "notas_master");
$conexion->query("SET NAMES 'utf8'");

//consulta a paginar
$consuta = $conexion->query("SELECT * FROM notas");
$numFilas= $consuta->num_rows;
$numElementoPagina = 2;


//hacer paginacion
$pagination = new Zebra_Pagination();

//numero total de elementos
$pagination->records($numFilas);

//numero de elementos por pagina
$pagination->records_per_page($numElementoPagina);

