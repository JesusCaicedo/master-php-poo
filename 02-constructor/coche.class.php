<?php

//POO EN PHP
//ELEMENTO PRINCIPAL DE LA POO SON LAS CLASES
//atributo es una variable

//definir una clase

class Coche{

    //atributo o propiedades es una variable
    public $color;
    private $modelo;
    protected $marca;
    
    public $velocidad;
    public $caballaje;
    public $plazas;


    //constructor nunca devuelve datos

    public function __construct($color,$marca,$modelo,$velocidad,$caballaje,$plazas){
        $this->color = $color;
        $this->modelo = $modelo;
        $this->marca = $marca;
        $this->velocidad = $velocidad;
        $this->caballaje = $caballaje;
        $this->plazas = $plazas;
    }



    //metodos son acciones - funciones
    //get recolectar y mostrar
public function getColor(){
    //this significa buscar en la clase
    return $this->color;
}


//set modificar
public function setColor($color){
$this->color = $color;
}


//metodo
public function acelerar(){
    $this -> velocidad++;
}

public function getModelo(){
    return $this->modelo;
}

public function setModelo($modelo){
    $this->modelo = $modelo;
}

public function setMarca($marca){
    $this->marca = $marca;
}

//metodo
public function frenar(){
    $this -> velocidad--;
}

public function getVelovidad(){
    return $this->velocidad;
}


public function mostrarInformacion(Coche $miObjeto){

    if(is_object($miObjeto)){
        $informacion = "<h1>informacion del coche: </h1>";
        $informacion.= "<br> color ".$miObjeto->color;
        $informacion.= "<br> Modelo ".$miObjeto->modelo;
        $informacion.= "<br> Velocidad ".$miObjeto->velocidad;

    return $informacion;

    }else{
        $informacion = "en dato no es de tipo Coche";

    }

    
}

} //fin de la clase


?>