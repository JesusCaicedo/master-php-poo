<?php
//HERENCIA COMPARTE ATRIBUTS Y METODOS ENTRE CLASES

class Persona{
    
public $nombre;
public $apellidos;
public $altura;
public $edad;

//GET
public function getNombre(){
    return $this->nombre;
}
public function getApellidos(){
    return $this->apellidos;
}
public function getAltura(){
    return $this->altura;
}
public function getEdad(){
    return $this->edad;
}
//SET
public function setNombre($nombre){
    $this->nombre = $nombre;
}
public function setApellidos($apellidos){
    $this->apellidos = $apellidos;
}
public function setAltura($altura){
    $this->altura = $altura;
}
public function setEdad($edad){
    $this->edad = $edad;
}



public function hablar(){
    return "estoy hablando";
}
public function caminar(){
    return "estoy caminando";
}


}

class Informatico extends Persona{


    public $lenguajes;

    public function __construct(){
        $this->lenguajes = "html,cc y js";

    }


    public function sabeLenguajes($lenguajes){
        $this->lenguajes = $lenguajes;
        return $this->lenguajes;
    }

    public function programar(){
        return "soy progrmador";
    }

    public function repararOrdenador(){
        return "reparar ordenadores ";
    }
    public function hacerOfimatica(){
        return "hago ofimatica";
    }
}
class TecnicoRedes extends Informatico{

    public $auditarRedes;
    public $experienciaRedes;

    public function __construct(){
        //llamo al constructor padre para heredar sus valores
        parent::__construct();
        $this->auditarRedes="experto";
        $this->experienciaRedes =5;
    }


    public function auditoria(){
        return "estoy editando la auditoria";
    }
}

?>