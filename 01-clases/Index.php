<?php

//POO EN PHP
//ELEMENTO PRINCIPAL DE LA POO SON LAS CLASES
//atributo es una variable

//definir una clase

class Coche{

    //atributo o propiedades es una variable
    public $color = "rojo";
    public $modelo = "aventador";
    public $marca = "ferrari";
    public $velocidad = 300;
    public $caballaje = 500;
    public $plazas = 2;


    //metodos son acciones - funciones


    //get recolectar y mostrar
public function getColor(){
    //this significa buscar en la clase
    return $this->color;
}


//set modificar
public function setColor($color){
$this->color = $color;
}


//metodo
public function acelerar(){
    $this -> velocidad++;
}

public function getModelo(){
    return $this->modelo;
}

public function setModelo($modelo){
    $this->modelo = $modelo;
}

//metodo
public function frenar(){
    $this -> velocidad--;
}

public function getVelovidad(){
    return $this->velocidad;
}

} //fin de la clase

//crear objeto o instaciar la clase

$coche = new coche();

//usar los metodos

echo $coche -> setColor('verde');
echo "el color del carro1 es ".$coche -> getColor()."<br>";
$coche->frenar();
echo "la velocidad del coche es ".$coche->getVelovidad()."<br>";
$coche->setModelo("9999");
echo "el modelo es ".$coche->getModelo()."<br><br>";

$coche2 = new coche();

echo $coche -> setColor('gris');
echo "el color del carro2 es ".$coche -> getColor()."<br>";
$coche->acelerar();
echo "la velocidad del coche es ".$coche->getVelovidad()."<br>";
$coche->setModelo("2000");
echo "el modelo es ".$coche->getModelo()."<br>";

var_dump($coche2);

?>